#include "funkcje.h"
#include <math.h>

float odch(float *p_tab)
{
    float sumisrd = 0.0;
    float odchylenie = 0.0;

    for(int i=0; i<50; i++)
    {
        sumisrd += p_tab[i];
    }

    sumisrd /= 50;

    for(int i=0; i<50; i++)
    {
        odchylenie += (p_tab[i] - sumisrd)*(p_tab[i] - sumisrd);
    }

    odchylenie /= 50;

    return sqrt(odchylenie);
}